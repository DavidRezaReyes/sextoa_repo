class ZombisController < ApplicationController
  before_action :set_zombi, only: [:show, :edit, :update, :destroy]

  # GET /zombis
  # GET /zombis.json
  def index
    @zombis = Zombi.all
  end

  # GET /zombis/1
  # GET /zombis/1.json
  def show
  end

  # GET /zombis/new
  def new
    @zombi = Zombi.new
  end

  # GET /zombis/1/edit
  def edit
  end

  # POST /zombis
  # POST /zombis.json
  def create
    @zombi = Zombi.new(zombi_params)

    respond_to do |format|
      if @zombi.save
        format.html { redirect_to @zombi, notice: 'Zombi was successfully created.' }
        format.json { render :show, status: :created, location: @zombi }
      else
        format.html { render :new }
        format.json { render json: @zombi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /zombis/1
  # PATCH/PUT /zombis/1.json
  def update
    respond_to do |format|
      if @zombi.update(zombi_params)
        format.html { redirect_to @zombi, notice: 'Zombi was successfully updated.' }
        format.json { render :show, status: :ok, location: @zombi }
      else
        format.html { render :edit }
        format.json { render json: @zombi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /zombis/1
  # DELETE /zombis/1.json
  def destroy
    @zombi.destroy
    respond_to do |format|
      format.html { redirect_to zombis_url, notice: 'Zombi was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_zombi
      @zombi = Zombi.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def zombi_params
      params.require(:zombi).permit(:name, :age)
    end
end
