json.extract! zombi, :id, :name, :age, :created_at, :updated_at
json.url zombi_url(zombi, format: :json)
