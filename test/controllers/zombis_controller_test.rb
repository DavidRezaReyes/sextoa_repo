require 'test_helper'

class ZombisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @zombi = zombis(:one)
  end

  test "should get index" do
    get zombis_url
    assert_response :success
  end

  test "should get new" do
    get new_zombi_url
    assert_response :success
  end

  test "should create zombi" do
    assert_difference('Zombi.count') do
      post zombis_url, params: { zombi: { age: @zombi.age, name: @zombi.name } }
    end

    assert_redirected_to zombi_url(Zombi.last)
  end

  test "should show zombi" do
    get zombi_url(@zombi)
    assert_response :success
  end

  test "should get edit" do
    get edit_zombi_url(@zombi)
    assert_response :success
  end

  test "should update zombi" do
    patch zombi_url(@zombi), params: { zombi: { age: @zombi.age, name: @zombi.name } }
    assert_redirected_to zombi_url(@zombi)
  end

  test "should destroy zombi" do
    assert_difference('Zombi.count', -1) do
      delete zombi_url(@zombi)
    end

    assert_redirected_to zombis_url
  end
end
