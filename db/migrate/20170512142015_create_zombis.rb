class CreateZombis < ActiveRecord::Migration[5.0]
  def change
    create_table :zombis do |t|
      t.string :name
      t.integer :age

      t.timestamps
    end
  end
end
