class AddRottenToZombis < ActiveRecord::Migration[5.0]
  def change
    add_column :zombis, :rotten, :boolean
  end
end
