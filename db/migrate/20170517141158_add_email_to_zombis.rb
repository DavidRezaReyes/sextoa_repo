class AddEmailToZombis < ActiveRecord::Migration[5.0]
  def change
    add_column :zombis, :email, :string
  end
end
